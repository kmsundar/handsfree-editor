import {CommandRecognizer} from './CommandRecognizer.mjs';
import {NativeSpeechRecognizer} from './NativeSpeechRecognizer.mjs';

class App {
    constructor(win, doc) {
        const self = this;

        this.win = win;
        this.doc = doc;
        this.trix = doc.querySelector('trix-editor');
        this.command = new CommandRecognizer();
        this.command.subscribe(function undo() {
            App.undo(self.trix);
        });
        this.command.subscribe(function redo() {
            App.redo(self.trix);
        });
        this.speech = new NativeSpeechRecognizer(this.win, (text) => {
            if (self.command.isCommand(text) === true) {
                self.command.execute(text);
            }
            else {
                App.insertText(self.trix, text);
            }
        });
    }

    static showLog(message) {
        const con = console;

        con.log(message);
    }

    static normalizeText(trix, text) {
        const editor = trix.editor,
            fulltext = editor.getDocument().toString().trim(),
            selection = editor.getSelectedRange();
        let startwith, endwith;

        if (text.startsWith(' ') === false && selection.length > 0) {
            startwith = fulltext.substring(0, selection[0]);
            if (startwith.length > 0 && startwith.endsWith(' ') === false) {
                text = ' ' + text;
            }
        }
        if (text.endsWith(' ') === false && selection.length > 1) {
            endwith = fulltext.substring(selection[1]);
            if (endwith.length > 0 && endwith.startsWith(' ') === false) {
                text += ' ';
            }
        }

        return text;
    }

    static insertText(trix, text) {
        const editor = trix.editor;

        App.showLog('Got some text: "' + text + '"');
        editor.recordUndoEntry("insert-text");
        text = App.normalizeText(trix, text);
        editor.insertString(text);
    }

    static undo(trix) {
        trix.editor.undo();
    }

    static redo(trix) {
        trix.editor.redo();
    }

    start() {
        App.showLog('App Started..');
        this.speech.start();
    }
}

new App(window, document).start();
