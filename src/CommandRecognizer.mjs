class CommandRecognizer {
    constructor() {
        this.commands = {
            'wiki undo': 'undo',
            'vicky and do': 'undo',
            'vicky and': 'undo',
            'vicky and you': 'undo',
            'wiki redo': 'redo',
            'wiki de do': 'redo',
            'wiki Lado': 'redo',
            'wiki read': 'redo',
            'wiki reader': 'redo',
            'vicky red off': 'redo'
        };
        this.callbacks = [];
    }

    isCommand(text) {
        return this.commands.hasOwnProperty(text.trim().toLowerCase());
    }

    isAction(callback) {
        return Object.values(this.commands).includes(callback.name);
    }

    getActionName(text) {
        if (this.isCommand(text) === true) {
            return this.commands[text.trim().toLowerCase()];
        }

        return null;
    }

    execute(text) {
        const action = this.getActionName(text);
        let callback = null;

        if (action !== null) {
            this.callbacks.every((item) => {
                if (item.name === action) {
                    callback = item;

                    return false;
                }

                return true;
            });
            if (callback === null) {
                return false;
            }
            callback();

            return true;
        }

        return false;
    }

    subscribe(callback) {
        if (this.callbacks.includes(callback) === false &&
            this.isAction(callback) === true
        ) {
            this.callbacks.push(callback);
        }
    }

    unsubscribe(callback) {
        if (this.callbacks.includes(callback) === true) {
            this.callbacks.slice(this.callbacks.indexOf(callback), 1);
        }
    }
}

export {CommandRecognizer};
