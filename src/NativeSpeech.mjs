class NativeSpeech {
    constructor(win, callback) {
        this.win = win;
        this.callback = callback;
        this.recognizer = null;
        this.init();
    }

    static showLog(message) {
        const con = console;

        con.log(message);
    }

    static getRecognizer(win) {
        const SpeechRecognition = win.SpeechRecognition || win.webkitSpeechRecognition,
            SpeechGrammarList = win.SpeechGrammarList || win.webkitSpeechGrammarList,
            speechRecognitionList = new SpeechGrammarList(),
            recognition = new SpeechRecognition(),
            grammar = '#JSGF V1.0;';
        
        speechRecognitionList.addFromString(grammar, 1);
        recognition.grammars = speechRecognitionList;
        recognition.continuous = true;
        recognition.lang = 'en-US';
        recognition.interimResults = false;
        recognition.maxAlternatives = 1;

        return recognition;
    }

    start() {
        if (this.recognizer !== null && typeof this.recognizer !== 'undefined') {
            this.recognizer.start();
        }
    }

    stop() {
        if (this.recognizer !== null && typeof this.recognizer !== 'undefined') {
            this.recognizer.stop();
        }
    }

    restart() {
        if (this.recognizer !== null && typeof this.recognizer !== 'undefined') {
            this.stop();
            this.init();
            this.start();
        }
    }

    init() {
        const self = this;

        this.recognizer = NativeSpeech.getRecognizer(this.win);
        this.recognizer.onresult = (event) => {
            let text = '';

            for (let i = event.resultIndex; i < event.results.length; i += 1) {
                text += event.results[i][0].transcript;
            }
            self.callback(text);
        };
        
        this.recognizer.onspeechend = () => {
            self.restart();
        };
        
        this.recognizer.onnomatch = () => {
            NativeSpeech.showLog('Error: No Match Found !');
            self.restart();
        };
        
        this.recognizer.onerror = (event) => {
            NativeSpeech.showLog('Error: ' + event.error);
            self.restart();
        };
    }
}

export {NativeSpeech};