### What is this repository for? ###

* Its voice based text editing tool


### Install Node ###

* wget --no-check-certificate -q -O - https://deb.nodesource.com/setup_14.x | bash -
* sudo apt-get update --fix-missing
* sudo apt-get install -y nodejs


### How do I get set up? ###

* Clone repo from Bitbucket
* cd (downloaded-path)
* npm install
* npm start
* Note: After 'npm start' command a new local server will be booted in 9090 port (if you want to change the port -> do it in package.json), Just click the link in your console 


