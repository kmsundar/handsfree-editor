import express from 'express';
import path from 'path';

const app = express();
let host, port;

if (process.argv.length !== 4) {
    throw new Error('app.starter.invalid.command.line.argument');
}
if (process.argv.length > 2) {
    host = process.argv[2];
}
if (process.argv.length > 3) {
    port = process.argv[3];
}

function showLog(message) {
    const con = console;

    con.log(message);
}

app.use(express.static(process.cwd()));
app.use('/trix', express.static(path.join(process.cwd(), 'node_modules/trix/dist')));

app.listen(port, (error) => {
    if (typeof error !== 'undefined') {
        showLog('Error Listening Port ' + port + ' !!!');

        return;
    }
    showLog('Click http://' + host + ':' + port);
});